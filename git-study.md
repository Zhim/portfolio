#2019.08.26 git sutdy
<hr>
<hr>

### 1. Start

* __Figure 1. Local Version Control__
  * ex) RCS(Revision Control System)
    * *use patch set file
<br>
* __Figure 2. Center Version Control__
  * use Center VCS Server with Clients
  * All Client can know what other clients do 
  * There is no way to backup project history when Center VCS Server is down.
<br>
* __Figure 3. Distributed Version Control__
* ==clone== project files from server
    * __clone has all of information of project file and history__
-----
###2. Using Git

* __3Status of gitProject files__
  1. Committed
       - data have saved in local database 
  2. Modified
       - Modified data have not committed to local database 
  3. Staged
        - Indicates that a modified file is ready for commit
<br>
* __git Commands__
    * make git repository
        * mkdir directory-name
        * cd directory-name
        * git init
          * this command create a new subdirectory named ==.git== that contains all of your nedessary repository files
    * register files to version control
      * git add file-name
      * git commit -m 'commit message'
    * cloning an existing repository
      * cd directory that you want use for git repository
      * git clone address of remote repository
    * checking the status of your files
      * git status
        ```
        RESULT
        On branch master
        your branch is up-to-date with 'origin/master'.
        nothing to commit, working directory clean.
        ```
    * tracking new files
      * add new-file-name