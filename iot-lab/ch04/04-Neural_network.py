import tensorflow as tf
import numpy as np

x_data = np.array(
            [[0, 0], [1, 0], [1, 1], [0, 0], [0, 0], [0, 1]])

y_data = np.array([
        [1, 0, 0],
        [0, 1, 0],
        [0, 0, 1],
        [1, 0, 0],
        [1, 0, 0],
        [0, 0, 1]
])



x = tf.placeholder(tf.float32)
y = tf.placeholder(tf.float32)

w = tf.Variable(tf.zeros[3])

L = tf.add(tf.matmul(x,W),b)
L = tf.nn.relu(L)


model = tf.nn.softmax(L)

cost = tf.reduce_mean(-tf.reduce_sum(Y*tf.log(model),axis-1))

optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.01)
train_op = optimizer.minimize(cost)


