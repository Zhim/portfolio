# Solve err

----
### CPU instructions Warning

- warning message
``` 
I tensorflow/core/platform/cpu_feature_guard.cc:140] Your CPU supports instructions that this TensorFlow binary was not compiled to use: AVX2 FMA
```

- solution

    1. ignore warning messge. 
        - set 'TF_CPP_MIN_LOG_LEVEL' = 2 that manage tensorflow warning log level.
        - solution
        ```python
             #solution 1
             import os
             os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

              #solution 2
              set TF_CPP_MIN_LOG_LEVEL = 2 before run tensorflow python file.

        ```

    2. rebuild tensorflow binary to fit your excute env. 
        - reference here : https://github.com/tensorflow/tensorflow/issues/8037

      


    
