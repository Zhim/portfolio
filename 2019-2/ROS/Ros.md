# Objected Oriented Modeling
ver.ubuntu 16.04 ROS [python 2.7]

----
----
python2.x : ASCII
python3.x : UTF-8
python 3.5 typing
```python
def foo (n : int)-> int: // interpreter can know that value of type
    ...
```

```python
file name : hello-python

#! /usr/bin/env python
# python 2.n 환경을 찾아서 실행하라. (쉬뱅?)
#unix 운영체제에서 1라인의 주석은 의미가 있음.

if__name__=="__main__":
    say_hello()


"""__name__은 시스템 정의 이름으로 표준 입력 또는 상호작용 프롬폼트로 부터 읽혀져 
실행될 때 모듈의  __main__을 찾아 실행, 되도록 위와 같은 유형으로 사용할 것.
직접 hello.py등의 이름으로 호출하여 단독 실행하는 경우와
다른 프로그램에서 import 될때 지정되는 이름에 차이를 두어 사용할 수 있다.
"""

def say_hello(name):
    if name == "":
        print("hello")
    else:
        print("hello %s",%name)
        """'%'를 사용할 때는 변수가 하나일때는 괄호 없이, 
        두개 이상일 때는 %(a,b) 와 같이 튜플로 이용"""

#Python class

class Greeter :
    tmp = 12 # 생성자 밖에 선언하면 클래스변수(=static value)가 된다.
    def __init__(self,a,b): #생성자. 클래스 당 1개만 선언가능
        pass
        #인스턴스 변수 정의
        self.a = 10
        self.b = 2 
        
        
    def say_hello(self):
        print("hello, world!")

    if __name__ =="__main__":
        gildong = Greeter()
        gildong.say_hello()
```

-----
###2019.09.18

####1장
실행 되는 프로그램 - node
topic 이라는 것으로 메세지를 전달
... -> 1장 2장 공부하기

-----
2019.09.20
- 유스케이스
  - 고수준 유스케이스의 예시
    - 유스케이스 이름 : process sale
    - 액터 : 고객, 판매원
    - 유형 : 기본
    - 설명 : 고객은 구매하고자 하는 물품을 가지고 판매원이 있는 계산대로 간다. 판매원은 구매 항목을 기록하고 지불을 처리한다. 계산이 완료되면 고객은 매장을 떠난다.

  - 확장 유스케이스 -> 작업 후에 작업이 제대로 진행되었는지 확인하는 용도로 사용가능
    - 유스케이스 이름
    - 액터
    - 목적
    - 개요
    - 유형
    - 참조
    - 이벤트 흐름 or 주 흐름 - 액터 | 시스템
    - 대체 이벤트
  
- 좋은 유스케이스의 조건
  1. 액터가 반드시 확인되어야 한다.
  2. 시작 조건이 중요
  3. 끝나는 조건이 확실해야 한다.
  4. 일반화 된 시나리오를 가지고 있어야 한다.

- 유스케이스의 유형 
  - 핵심 유스케이스와 실제 유스케이스로 나눠짐.
  - 핵심 유스케이스로 작성하다. 실제 유스케이스에 대해 작성


- 유스케이스의 우선순위
  
- 도메인 모델 (가장 중요!!)
  - 객체지향 분석 과정에서 작성하여야 하는 가장 중요한 산출물
  - 문제 도메인의 의미 있는 개념적인 클래스들.
  - 개념모델 도메인 객체 모델 분석 객체 모델 등으로 불림
  - UML 도메인 모델은 오퍼레이션을 정의하지 않는 일련의 다이어그램으로 표현
    - 도메인객체 혹은 개념적 클래스
    - 개념적클래스 간의 연관 관계
    - 개념적 클래스 간의 연관 관계
    - 개념적 클래스의 애트리뷰트
  - 소프트웨어 설계가 아닌 실세계의 문제 도메인에 존재하는 개념적 클래스를 설명->
    - 데이터베이스나 윈도우 등과같이...

- 구조적 분석에서는 분해의 초점이 프로세스 혹은 기능이나 객체지향 분석(UP) 에서는 개념적 클래스 혹은 객체가 분해의 초점이 됨.
- 분석 단계에서는 문제 도메인에 존재하는 다양한 개념적 클래스를 식별하고 이들을 도메인 모델로서 문서화 하여야 함.

- 도메인 모델 작성 절차
    1. 개념적 클래스 범주 리스트와 명사구 식별방법을 사용하여 현재 고려중인 요구사항에 관련된 후보 개념적 클래스들을 식별한다.
    2. 개념적 클래스들을 표시
    3. 연관관계 표시

- 연관관계 식별시 유의사항
  - 연관관계를 식별하는 것보다 개념적 클래스를 식별하는 것이 중요!

- 좋은 클래스
  - 단 하나의 추상화
    - 하나의 클래스는 하나의 주요한 주제를 가진다.

  - 도메인 용어를 사용하여 이름을 짓는다.
    - 명사 : 추상화를 잘 표현
    - 약어 사용(클래스 문서화에서 full name 명시)

- 명사식별
  - 유스케이스를 통해 
  - 요구사항 분석 명세서로 부터
  - 제안서로 부터
  - 완성도가 높은 문서일 수록 개발자의 생각이 잘 반영된 산출물
  
항해성(navigability) (분석 시에는 나타내지 않고 설계 시에 나타냄)
:Dog (anonymous object)
100page 까지 했음

-----
-----

#ROS - 2019.09.25

- ros 프로그램은 반드시 roscore가 실행되고 있어야 함.
- 토커 노드 - > 토픽 이름 -> 리스너 노드
- rostopic list ( 현재 발행중인 토픽 목록 출력)
- rostopic echo /chatter (특정 토픽 보기)
- rostopic hz/chater (특정 토픽 발생 주기)
- 실행 된 프로그램 노드
- chatter:=talker01 -> 토픽 이름 재지정(이 방법으로 쓰는것을 추천)
- __name:=listener01 -> 노드 이름 변경
#### roslaunch
- 여러개의 ros 노드 시작을 자동화 하는 명령행 도구
- example.launch
- $rosce rospy_tutorials/
- cd 001_talker_listener/
- gedit talker_listener.launch
  
- 패키지 이름 중요!
- version major,regular,miner (1.0.0)
```python

import rospy
from std_msgs.msg import Int32

rospy.init_node('topic_publisher') #<매우 중요!!!!

pub = rospy.Publisher('counter',Int32)
# or pub = rospy.Publisher('cunter',Int32,queue_size =10)

rate = rospy,Rate(2)# 실행주기 : n Hz -> 2Hz
count = 0

while not rospy.is_shutdown(): # 프로그램이 종료되지 않으면 (ctrl+c 입력이 없으면) 계속 반복
    pub.publish(count)
    count +=1
    rate.sleep()#2 Hz에 맞게 실행 중지
    #time.sleep()은 차이가 날 수 있음! 따라서 rate.sleep()을 사용함 해당 코드 위쪽의 처리량에 따라 차이 큼!

*rospy.spin() #-> ros 노드가 shutdown 될때까지  콜백 함수가 종료되지 않도록 블록.
```
-----
#2019.10.11

- seps9.deu.ac.kr
- id : j+ 학번
- pw : j + 학번 + *!!
- 팀 저장소에서 각자 브랜치를 생성하여 작업하기
  
### Action

 - goal : 무언가 하라
   - duration time_to_wait :
 - Result : Goal 서버에서 행동한 내용을 결과를 받음
   - uint32 updates_sent
 - Feedback : 적정한 주기로 로봇의 상태를 알려줄 수 있음
   - duration time_elapsed
   - duration time_remaining
- rospy.init_node() # 노드이름 등록
- time_to_sleep 만 잘 수정하면 됩니당
- auto start 읽기
- 주요 메소드들 pdf 읽어보기
- rospy.loginfo()#메세지 레벨 조절할 필요가 있음.
- set sucessed / aborted : 서로 반대의 경우에 대해 액션 목표 중지 설정
- pdf p.19 set_succeeded 와 set aborted의 차이(결국 if 문에서 실행 조건을 탐지하는데 무슨 차이가 있을까?)
- p 20로직 확인
- base link 로봇의 특정지점에서 기준점이되는 부분
- base footprint : baselink에서 지면까지의 법선을 그었을 때의 교점
- joint - link  관절?
- position : x, y, z direction
  - x : 이동방향 (m)
  - y : 
  - z : z축을 기준으로 회전하면 커브길 이동가능(rad) 
  
- orientatation   -> x,y,z ,w (quaternian - 사원수)
- costmap : 로봇 4m * 4m 내에서 이동가능한지를 판단함.
-----
# 2019.10.16

### middle term
- ros 60 python 40

- 예외 처리, 람다 표현식, 정규 표현식, numpy 사용
- ananymus object / func
- mapreduce filter
- regular expration for process string data
- numpy vector array process
- Sequence
- 집합 자료형 (set type)
- 사상 자료형 (dictionary)
- pdf p.9
- p.41 사용자 정의 예외 처리!
- fstring

------ 
# 2019.10.18

### map -filter - reduce


- map을 통해 원하는 요소만 추출.
```python
data= list (range (10));

data = list (map(lambda x: x+1,data))
from functools import reduce
result = reduce ((lambda x,y:x+y),data)
less_than_five = list(filter(lambda x: x < 5,data))

```
...
p. 45예제 기억

------
### Regular expressions

- 메타문자 [...]
  - 처리를 원하는 문자 집합을 나타내는 문자 클래스
  - [a,b,c], [a-c] : a or b or c
  - [a-z] : a~z 알파벳
  
-----
##### 2019.11.06

ros parameter :  로스 콜백 과는 다르게, 노드간에 공유할 수 있는 변수로 이용됨.
콜백 : 특정 토픽이 발행되었을 때(이벤트가 발생할 때) 바로실행,
파라미터 : 해당 변수가 변경되면 이를 검사한 이후에 그에 대한 작업 수행

-----
##### 2019.11.07
###### turtle bot bumper event

- 터틀봇은 오른쪽 중간 왼쪽 범퍼가 있음
  - rosmsg info kobuki_msgs/BumperEvent
    - left, center, right, realesed, pushed ( 0,1,2,0,1 )
  - rostopic list 명령어 확인
    - mobile_base  관련 메세지 확인하기

- rosbag
  - ros 메세지를 저장하고 재생해 주는 도구 : record, paly, info 등
  - bag 파일을 생성해서 유지보수 능률 향상 (변수 일일히 바꾸지 않아도 됨.)
  - 지도 만들기가 가능!

- base link
  - z 값이 0 인지점에서 터틀봇의 원둘레를 표시했을때 그 원의 중심
- base footprint 
  - 로봇이 지나간 지점을 찍는당

-----
#### 프로젝트 설명

obstacle spawn.py 경로 변경 해야함 : 런치파일로 만들어 줘서 우리가 안해도 댐
첫번째 만나는 사거리에서 신호등 보고 직진
S 자 통과
두번째 사거리 신호등 보고 직진
세번째 좌회전 정지표지판 보고 3초뒤 출발


로컬라이제이션
2d pose estimate
----
### chapter 10 navigation
Dijkstra
A*
Oilo distance
manheton distance ( 건물 직각 배열 )
-----
### 2019.11.15

- ros parameter is sended by roscore server<!--  -->