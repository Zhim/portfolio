#DataBase 2019. 09~
-----
-----
###2019.09.11
시나리오 -> 요구분석 -> ERD
ERD는 사이클로 돌아가도록 그리는 것이 좋다.
주제선정 : 헬스
시나리오, ERD 중간고사 전까지


-----
### 2019.11.12

#### Cursor

- 명시적 커서 
- 묵시적 커서

#### cursor for lop

``` sql
declare
    cursor emp_cur is
        select employee_id, first_name
        from employees
        where department_id = 100;
begin
    for emp_rec in emp_cur
        loop
            dbms_ouput.put_line(emp_rec.employee_id ||'  ' ||emp)
        end
```

#### explicit cursor
```sql
cursor emp_cur(p_dno employees.department_id%TYPE)
is
    select employee_id, first_name,salary
    from employees
    where department_id = p_dno; 
    /* 커서 선언 끗*/

    begin
    ...
    open emp_cur(20); /*커서 파라미터로 들어가서 호출됨.*/

```
감동; 울겠노 ㅋㅋ

no wait : 커서 선언시 for update로 행을 잠금

where current of emp_cur -- 커서 선언시에 잠긴 행 갱신함

### 예외처리

- 선언부
- 실행부
- 예외처리부


- 
- ----
   - 예외처리 종류
   - -  ACCESS_INTO_NULL 
                                : 정의되지 않은 오브젝트 속성에 값을 할당하고자 했을 때 발생하는 예외.                      
                            - CASE_NOT_FOUND 
                                : CASE문의 when절에 해당되는 조건이 없고 else절도 없을 경우 발생
                            - COLLECTION_IS_NULL 
                                : 선언되지 않은 컬렉션(nested table, varray)에 존재하는 메서드
                                                   이외의 메서드를 사용했을 때 발생되는 예외.
                            - CURSOR_ALREADY_OPEN 
                                : 이미 열려진 커서를 열려고 시도 했을 때 발생하는 예외
                            - DUP_VAL_ON_INDEX 
                                : 유일인덱스에 중복값을 입력햇을 때 발생하는 예외.
                            - INVALID_CURSOR 
                                : 잘못된 커서 조작이 샐행될 때 발생되는 예외.
                            - INVALID_NUMBER 
                                : 문자를 숫자로의 변환 시 실패가 될 때 발생하는 예외.
                            - LOGIN_DENIED 
                                : 잘못된 사용자명이나 암호로 로그인시도시 발생하는 예외.
                            - NO_DATA_FOUND 
                                : PL/SQL Select문이 한 건도 리턴하지 못하는 경우 발생하는 예외.
                            - NOT_LOGGED ON 
                                : 접속되지 않은 상태에서 데이터베이스에 대한 요청이 PL/SQL 프로그램으로
                                  실행된 경우 발생되는 예외.
                            - PROGRAM_ERROR 
                                : PL/SQL이 내부적인 문제를 가지고 있는 경우 발생되는 예외.
                            - ROWTYPE_MISMATCH 
                                : 할당문에서 호스트 커서 변수와 PL/SQL 커서 변수의 데이터 형이 불일치할 때 발생되는 예외
                            - STORAGE_ERROR 
                                : PL/SQL이 실행될 때 메모리가 부족하거나 메모리상에 문제가 일어났을 대 발생하는 예외.
                            - SUBSCRIPT_BEYOND_COUNT 
                                : 컬렉션의 요소 갯수보다 더 큰 첨자 값으로 참조한 경우 발생
                            - SUBSCRIPT_OUTSIDE_LIMIT 
                                : 컬렉션의 첨자 한계를 벗어난 참조가 일어났을 때 발생
                            - SYS_INVALID_ROWD 
                                : 문자열을 ROWID로 변환할 때 무효한 문자열의 표현일 경우 발생되는 예외.
                            - TIMEOUT_ON_RESOURCE 
                                : 자원에 대한 대기시간이 초과했을 때 발생하는 예외.
                            - TOO_MANY_ROWS 
                                : PL/SQL select문이 두건이상의 행을 리턴햇을 때 발생되는 예외.
                            - VALUE_ERROR 
                                : 산술,변환,절삭 크기 제약에 에러가 생겼을 때 발생되는 예외.
                            - ZERO_DIVIDE
                                : 0으로 나누려 했을 때 발생하는 예외.
### PRAGMA

```sql
new_msg EXEPTION
PRAGMA EXCEPTION_INIT(new_msg, -1);
```
-----
create or replace function find_dname (v_deptno in number)
return varchar2
is
v_dname dept.dname%type;
begin
    select dename into v_dname
    from dept where deptno = v_deptno;
    return(v_dname);
end find_dname;
/
show error;
### Trigger

- dml trigger
  - before trigger
  - after trigger
  - instead of trigger