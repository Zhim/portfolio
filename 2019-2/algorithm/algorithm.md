# ALGORITHM
-----
## 2019.10.08 TUE

#### HEAP p.65 ~
    - 우선순위 Queue로, 자료가 삭제될 때 우선순위에 따라 삭제 되는 큐
      - 완전 이진트리
      - 각 노드의 값은 자식의 값보다 크거나 같다 -> 최대값 heap (작거나 같으면 최소값 트리) 
      - 1차원 배열을 통해 구현

    - Heaq Sort
      - random data (1 direction array) -> initial heap 
        - > convert to heap
      - root node is max value (0 index in array)
        - heap setting
  max|...|min| 
  -|-|-|
   min -> max heap sort (cf p.68)
