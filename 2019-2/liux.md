## System Programming-Linux

----
----
### 운영체제의 이해
- 운영체제의 목적과 구성
    - 운영체제의 조건
        - 처리능력 향상
        - 응답시간 단축
        - 사용가능성 향상
        - 신뢰도향상
    - 운영체제의 구분
        - 운영체제는 제어프로그램과 처리 프로그램으로 구성
        - 제어 프로그램은 감시프로그램, 작업관리 프로그램, 데이터 관리 프로그램으로 구성.
        - 처리 프로그램은 언어 번역 프로그램 서비스 프로그램으로 구성
-----
-----
##2019.09.19
###쉘 스크립트

######과제 있음

- 로그인 쉘 : 리눅스에 로그인 시 사용되는 쉘
- 환경변수 : 사용자가 각자 원하는 형태로 자신의 컴퓨터 사용환경을 설정해 두기 위한 변수들
    - 환경변수 출력 예시
      - $ echo $TERM
      - $ set TERM ~~~
      - bash 환경파일 수정하기
      - umask 007 - 파일 생성시 파일의 권한 지정 (숫자에 따라 다름)
- Meta character :  쉘에서 특수하게 사용하는 문자
- 파이프 라인
  - 한 명령의 출력을 다른 명령이나 쉘의 입력으로 연결하여 사용
  - 여러 명령을 연결하여 복잡하거나 큰 작업을 빠르고 쉽게 구성가능

-----
## 2019.09.24

expr  사용 에
#expr 1 + 2
3

조건 식
true : 0
false : 1

if then elif then else fi(fi : if의 반대임 (리얼))

- GCC
- 라이브러리
- 빌드 도구
  - make
  - Cmake
- 소스코드 관리 도구
  - svn
  - 구글 코드
  - git

- 소스프로그램의 실행 프로그램 화
  1. 소스파일
  2. 헤더파일
  3. 전처리기 (cpp -PreProcess)
  4. 컴파일러
  5. 어셈블리 파일
  6. 어셈플러
  7. 목적파일 (.obj)
  8. 오브젝트 파일 라이브러리
  9. 링커
  10. 실행파일
  

  - 링킹이라는 말을 쓰는 이유?
    - 각각의 object 파일에서 사용하고 있는 함수나 변수등을 주소를 통해 이어주기 때문.
  - 정적 링킹과 동적 링킹
    - 정적 링킹
      - 최종실행파일에 필요한 링크들을 미리 다 링킹한 후 프로그램 실행
        - 장점 : 실행파일만 있으면 별도의 파일없이 실행가능
        - 단점 : 실행파일의 크기가 커지고 라이브러리 갱신시 관련된 실행파일들을 모두 컴파일하여 갱신
    - 동적 링킹
      - 실행하려고 할때 필요한 프로그램 모듈들을 결합하여 실행 계속
        - 장점 : 실행파일의 크기가 작아져 메모리를 조금 차지
        - 단점 : 실행파일 외에 별도의 라이브러리 필요
      - UNIX,Linux계열에서 대부분 이러한 파일

-----
## 2019.10.01
- cmake 예제 해보기
